#!/usr/bin/env python
import math
import time
import numpy as np
import rospy
from duckietown_msgs.msg import Twist2DStamped, LanePose

class lane_control():
    def __init__(self):
        rospy.init_node("lane_controller_node", anonymous=False)
        #PUBLICATION
        self.pub_car_cmd = rospy.Publisher("~car_cmd", Twist2DStamped, queue_size=1)
        
        #SUBCRIBTION
        self.sub_lane_reading = rospy.Subscriber("~lane_pose", LanePose, self.PoseHandling, queue_size=1)
    
    def PoseHandling(self,msg):
        d = msg.d
        phi = msg.phi
        mov = Twist2DStamped()
        kp_d = 9.0
        kp_phi = 3.3

        d_error = kp_d * d
        phi_error = kp_phi * phi

        control = -1 * (d_error + phi_error)
        
        mov.v = 1.0
        mov.omega = control
        self.pub_car_cmd.publish(mov)

        


if __name__ == "__main__":
    lanecontrol = lane_control()
    rospy.spin()
