#!/usr/bin/env python

import rospy
from duckietown_msgs.msg import Twist2DStamped

if __name__ == '__main__':
    rospy.init_node('open_loop', anonymous=True)
    pub = rospy.Publisher('~car_cmd', Twist2DStamped, queue_size=1)
    rate = rospy.Rate(10) # 10hz
    t_start = rospy.get_time()
    while not rospy.is_shutdown():
        t = rospy.get_time()
        msg = Twist2DStamped()
        dt = t - t_start
        if dt > 10 and dt < 13:
            msg.v = 0.25
            msg.omega = 0.0        
        elif dt > 13 and dt < 17:
            msg.v = 0.25
            msg.omega = 10.0
        elif dt > 17 and dt < 20:
            msg.v = 0.25
            msg.omega = 0.0      
        elif dt > 20 and dt < 21:
            msg.v = 0.25
            msg.omega = 5.0
        elif dt > 21 and dt < 23:
            msg.v = 0.25
            msg.omega = 0.0
        elif dt > 23 and dt < 24:
            msg.v = 0.0
            msg.omega = 5.0
        else:
            msg.v = 0
            msg.omega = 0
        pub.publish(msg)
        rate.sleep()
