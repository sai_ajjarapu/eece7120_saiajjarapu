#!/usr/bin/env python

#NEEDS TO: 1. SUBSCRIBE TO THE "image_transformer_node/corrected_image/compressed"
#          2. PUBLISH THE YELLOW AND WHITE HOUGH TRANSFORM IMAGES IN THIER OWN TOPICS

import rospy
import numpy as np
import cv2
from sensor_msgs.msg import CompressedImage, Image
from color_and_line import lane_filter
from cv_bridge import CvBridge

class mp3():
    def __init__(self):
        rospy.init_node("mp3",anonymous = True)
        rospy.Subscriber("ducknorris/image_transformer_node/corrected_image/compressed", CompressedImage,self.imagetime, queue_size=1)
        self.pub_white = rospy.Publisher("~white_transform",Image, queue_size=1)
        self.pub_yellow = rospy.Publisher("~yellow_transform",Image, queue_size=1)
        rospy.Rate(30) # 10hz

    def imagetime(self,CompressedImages):
        print("It's working")
        br = CvBridge()
        #Convert the ROS image to CV2 image and pass that image to the lane filter function
        np_arr = np.fromstring(CompressedImages.data,np.uint8)
        image_np = cv2.imdecode(np_arr, cv2.IMREAD_COLOR)
        self.white, self.yellow = lane_filter(image_np)

        #Convert the CV2 image to ROS image and publish that image 
        white_cv_image = br.cv2_to_imgmsg(self.white, encoding="bgr8")
        yellow_cv_image = br.cv2_to_imgmsg(self.yellow, encoding="bgr8")
        
        self.pub_white.publish(white_cv_image)
        self.pub_yellow.publish(yellow_cv_image)
        
        print("Thank God")
        
if __name__ == "__main__":
    image_transform = mp3()
    rospy.get_time()
    rospy.spin()

        





    
    