#!/usr/bin/env python

#Listener / subscriber node

import rospy 
import random
from geometry_msgs.msg import Point
from eece_7120_hw2.msg import Num

def callback(msg):
    pos = msg.position
    format_msg = 'Detected obstacle {0} at postion {1}, {2}, {3}'
    format_msg = format_msg.format(msg.id, pos.x, pos.y, pos.z)
    rospy.loginfo(format_msg)

def Listener():
    rospy.Subscriber('registered_obstacles',Num,callback)

if __name__ == '__main__':
    rospy.init_node('node3',anonymous=True)
    Listener()
    rospy.spin()