#!/usr/bin/env python

# Talker/ Publisher node

import rospy 
import random
from geometry_msgs.msg import Point

stack = 100

def talker():
    pub = rospy.Publisher('obstacles_detected', Point, queue_size=10)
    rospy.init_node('node1',anonymous=True)
    rate = rospy.Rate(10) # 10 Hz
    while not rospy.is_shutdown():
        pt = Point()
        pt.x = random.randrange(stack)
        pt.y = random.randrange(stack)
        pt.z = random.randrange(stack)
        rospy.loginfo(pt)
        pub.publish(pt)
        rate.sleep()

if __name__ == '__main__':
    try:
        talker()
    except rospy.ROSInterruptException:
        pass