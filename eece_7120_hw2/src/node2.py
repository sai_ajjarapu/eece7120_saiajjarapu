#!/usr/bin/env python

#pub and sub node

import rospy 
from eece_7120_hw2.msg import Num
from geometry_msgs.msg import Point

id_num = 1

def callback(Point):
    global id_num
    format_msg = Num()
    format_msg.id = 'input_%05d'%id_num
    format_msg.position = Point
    pub = rospy.Publisher('registered_obstacles', Num, queue_size=10)
    pub.publish(format_msg)
    id_num += 1

def pub_and_sub():
    sub = rospy.Subscriber('obstacles_detected', Point, callback)
    rospy.spin()
           

if __name__ == '__main__':
    rospy.init_node('node2',anonymous=True)
    pub_and_sub()